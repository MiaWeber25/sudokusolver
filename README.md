# sudokuSolver

Semester Project for CSCI 112 - Sudoku Solver

Progress: 
Outlined algorithm and class structure (See sudokuSolverClassDiagram.jpeg) 
Implemented File I/O from givens.txt to read in data in correct format
Set up output for grid (this is temporary, will implement GUI later, but needed for testing purposes)

Next Steps: 
get values from givens.txt into the gameBoard
begin to write algorithm for finding available values
work on iterating through the arrays and "searching" over arrays
